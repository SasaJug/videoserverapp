/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.magnum.dataup.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;

@Entity
public class Video {

	public static VideoBuilder create() {
		return ReflectionBuilder.implementationFor(VideoBuilder.class).create();
	}

	public interface VideoBuilder extends Builder<Video> {
		public VideoBuilder withTitle(String title);
		public VideoBuilder withDuration(long duration);
		public VideoBuilder withContentType(String contentType);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String title;
	private long duration;
	private String contentType;
	private double rating = 0.0;
	
	// We don't want to bother unmarshalling or marshalling
	// any owner data in the JSON. Why? We definitely don't
	// want the client trying to tell us who the owner is.
	// We also might want to keep the owner secret.
	@JsonIgnore
	private String owner;
	
	@JsonIgnore
	private Long usersRated = 0L;

	@JsonIgnore
	private String dataUrl;


	public long getId(){
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}


	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	@JsonProperty
	public String getDataUrl() {
		return dataUrl;
	}

	@JsonIgnore
	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}
	

	@JsonIgnore
	public Long getUsersRated() {
		return usersRated;
	}

	@JsonIgnore
	public void setUsersRated(Long usersRated) {
		this.usersRated = usersRated;
	}


	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their title and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		return Objects.hash(getTitle(), getDuration());
	}


	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their title and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Video)
				&& Objects.equals(getTitle(), ((Video) obj).getTitle())
				&& getDuration() == ((Video) obj).getDuration();
	}


}
