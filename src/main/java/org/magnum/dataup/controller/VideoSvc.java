package org.magnum.dataup.controller;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.magnum.dataup.VideoFileManager;
import org.magnum.dataup.VideoSvcApi;
import org.magnum.dataup.model.Video;
import org.magnum.dataup.model.VideoRepository;
import org.magnum.dataup.model.VideoStatus;
import org.magnum.dataup.model.VideoStatus.VideoState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;


import retrofit.http.Multipart;

@Controller
public class VideoSvc {

	
	private List<Video> videosList = new ArrayList<Video>();
	
	private static final AtomicLong currentId = new AtomicLong(0L);
	
	VideoFileManager mVideoManager;
	
	@Autowired
	private VideoRepository videos;
	
	
	// Receives GET requests to /video and returns the current
	// list of videos in memory. Spring automatically converts
	// the list of videos to JSON because of the @ResponseBody
	// annotation.
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList(){

		return videosList;
		
	};
	

	
	// Receives POST requests to /video and converts the HTTP
	// request body, which should contain json, into a Video
	// object before adding it to the list.	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.POST)
	public synchronized @ResponseBody Video addVideo(@RequestBody Video v){
		
		checkAndSetId(v);
		setRating(v);
		v.setDataUrl(getDataUrl(v.getId()));
		
		videosList.add(v);
	    
		return v;
	}



	@Multipart
	@RequestMapping(value=VideoSvcApi.VIDEO_DATA_PATH, method=RequestMethod.POST)
	public @ResponseBody VideoStatus setVideoData(
			@PathVariable("id") long id,
			@RequestParam("data") MultipartFile videoData,
			HttpServletResponse resp)
	{
		
		VideoStatus status = new VideoStatus(VideoState.READY);
		
		Video target = getVideo(id);
		if (target == null){
			 try {
				resp.sendError(404, "Video data does not exist");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				mVideoManager = VideoFileManager.get();
				InputStream inputStream = videoData.getInputStream();

				mVideoManager.saveVideoData(target, inputStream);
				return status;					
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	
		return null;
	}
	
	
	
	
	@RequestMapping(value=VideoSvcApi.VIDEO_DATA_PATH, method=RequestMethod.GET)
	public void getData(@PathVariable("id") long id, HttpServletResponse resp){

		Video target = getVideo(id);
			 try {
				 mVideoManager = VideoFileManager.get();
				if (target == null || !mVideoManager.hasVideoData(target)){
					 resp.sendError(404, "Video does not exist");
				} else {
					OutputStream outputStream= resp.getOutputStream();
					mVideoManager.copyVideoData(target, outputStream);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	
	
	
	@RequestMapping(value=VideoSvcApi.VIDEO_RATING_PATH, method=RequestMethod.GET)
	public @ResponseBody Video rateVideo(@PathVariable(VideoSvcApi.ID_PARAMETER) long id,
										 @PathVariable(VideoSvcApi.RATING_PARAMETER) int rating)
	{
		
		Video target = getVideo(id);
		double usersRated = target.getUsersRated();
		double oldRating = target.getRating();
		double sum = 0;
		if (usersRated != 0){
			sum = (double) (usersRated * oldRating);
		}
		
		
		usersRated ++;
		sum = sum + Double.valueOf(rating);
		
		Double newRating = (double) (sum/usersRated);
		
		target.setRating(newRating);
		target.setUsersRated((long) usersRated);
		return target;

	}
	
	
	
	
	
	// Extracts Video object with same id and returns it. If no such Video is found - returns null.
	private Video getVideo(long id) {

		for(Video v : videosList){
			if(v.getId() == id){
				return v;
			}
		}
		return null;
	}



	// Returns url for given Video instance id parameter
    private String getDataUrl(long videoId){
        String base_url = getUrlBaseForLocalServer();
        String url =  base_url + "/video/" + videoId + "/data";
        return url;
    }

    // Returns url base for local server
    private String getUrlBaseForLocalServer() {
       HttpServletRequest request = 
           ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
       String base = 
          "http://"+request.getServerName() 
          + ((request.getServerPort() != 80) ? ":"+request.getServerPort() : "");
       return base;
    }

    // Sets id for Video instance
    private void checkAndSetId(Video entity) {
        if(entity.getId() == 0){
            entity.setId(currentId.incrementAndGet());
        }
    }
	
	private void setRating(Video entity) {
		entity.setRating(0.0);
	}


}
